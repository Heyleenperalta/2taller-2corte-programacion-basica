#Taller: 
# Generar un programa en python que dibuje en pantalla una linea:

import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

fig, ax = plt.subplots()

Path = mpath.Path
path_data = [
    (Path.MOVETO, (4, 5)),
    (Path.LINETO, (3.3, 3.3)),
    (Path.CURVE4, (3, 5)),
    ]
codes, verts = zip(*path_data)
path = mpath.Path(verts, codes)
x, y = zip(*path.vertices)
line, = ax.plot(x, y, 'go-')

ax.grid()
ax.axis('equal')
plt.show()

#2. Generar un programa en python que dibuje en pantalla un elipse:
import matplotlib.pyplot as plt
import matplotlib.patches as pc

fig1 = plt.figure() 
ax1 = fig1.add_subplot(111, aspect='equal')
plt.xlim(0,10)
plt.ylim(0,10)
ax1.add_patch(

    pc.Ellipse(  
        (6.5, 6.5),  
        5.6,  # Largo
        3.4,  # ancho  
        color='#8A2BE2'
    )
)
plt.show()